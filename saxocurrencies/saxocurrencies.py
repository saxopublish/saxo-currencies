# -*- coding: utf-8 -*-
from decimal import Decimal

import requests
from moneyed import Money


class CurrencyDoesNotExist(Exception):
    pass


class Currencies(object):
    API_RETRIEVE_CURRENCIES_URL_PATTERN = '{api_base_url}Currencies/Currencies.json?key={api_key}'
    API_RETRIEVE_CURRENCY_URL_PATTERN = '{api_base_url}Currencies/{currency_id}.json?key={api_key}'

    def __init__(self, api_url, api_key):
        self._api_url = api_url
        self._api_key = api_key
        self._retrieve_currencies_url = self.API_RETRIEVE_CURRENCIES_URL_PATTERN.format(
            api_base_url=api_url,
            api_key=api_key
        )

    def _retrieve_currency_url(self, currency_id):
        return self.API_RETRIEVE_CURRENCY_URL_PATTERN.format(
            api_base_url=self._api_url,
            currency_id=currency_id,
            api_key=self._api_key
        )

    def get_currency_by_code(self, currency_code):
        """
        Retrieves the currency data from Saxo.

        :param currency_code: Currency to figure out the data.

        :return: data from the shop.

        e.g.
            get_currency_by_code('USD')
        """
        # ensure we have it as str
        currency_code = str(currency_code)

        currencies = requests.get(
            self._retrieve_currencies_url
        ).json()['currencies']
        # take the currency if exist
        currencies = [
            currency for currency in currencies if
            currency['alias'] == currency_code
        ]
        # making sure we have a valid currency
        if not currencies:
            raise CurrencyDoesNotExist(
                "Currency {} doesn't exist in Saxo.".format(currency_code)
            )
        return currencies[0]

    def get_currency_by_id(self, currency_id):
        """
        Get the currency data from the shop according with the currency id.

        :param shop_currency_id: the currency id on the shop.
        :return: the currency data
        """
        saxo_response = requests.get(
            self._retrieve_currency_url(currency_id)
        )

        data = saxo_response.json()

        if saxo_response.status_code == 404 or \
                'currencies' not in data.keys():
            raise CurrencyDoesNotExist(
                'The currency with id {} does not exist in Saxo'.format(
                    currency_id
                )
            )

        return data['currencies'][0]

    def get_currency_code_by_id(self, currency_id):
        """
        Get the currency code from the shop according with the currency id.

        :param shop_currency_id: the currency id on the shop.
        :return: the currency code
        """
        return self.get_currency_by_id(currency_id)['alias']

    def convert_from_currency_data(
        self,
        amount_money,
        convert_to_currency_data,
        amount_money_currency_data
    ):
        """
        Convert amount to currency given the two currencies data dicts.

        :param amount: an instance of Money we want to convert from.
        :param convert_to_currency: the resulting currency we would
         like to convert to
        :param amount_money_currency_data: currency dict from saxo shop
         for amount_money.
        :param amount_to_convert_currency_data: currency dict from saxo
         shop for amount_to_convert
        """
        convert_to_currency = convert_to_currency_data["alias"]
        if str(amount_money.currency) == str(convert_to_currency):
            return amount_money

        return Money(
            amount=amount_money.amount * Decimal(
                amount_money_currency_data['exchangerate']
            ) / Decimal(
                convert_to_currency_data['exchangerate']
            ),
            currency=convert_to_currency
        )

    def convert(self, amount_money, convert_to_currency):
        """
        Convert the amount given to convert_to_currency.

        :param amount: an instance of Money
        :param convert_to_currency: the result currency we would like to
                convert the amount
        :return: the amount converted
        """
        if str(amount_money.currency) == str(convert_to_currency):
            return amount_money

        convert_to_currency_data = self.get_currency_by_code(
           convert_to_currency
        )

        # Get the currency data for the amount to convert currency
        amount_money_currency_data = self.get_currency_by_code(
            amount_money.currency
        )
        return self.convert_from_currency_data(
            amount_money,
            convert_to_currency_data,
            amount_money_currency_data
        )
