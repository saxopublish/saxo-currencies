# saxo-currencies

_This repository has [moved to GitLab](https://saxo.githost.io/publish/saxo-currencies)._

A Python utility for currency handling and conversions, using the Saxo Currency Service.

# Install
To install it, make sure you have Python 2.7 or greater installed. And use one of the following options:

* **Using pip**:

       To install from the most recent code:

            pip install git+ssh://git@github.com/SaxoCom/saxo-currencies.git

       From a specific version

            pip install git+ssh://git@github.com/SaxoCom/saxo-currencies.git@<release-tag>

            e.g: pip install git+ssh://git@github.com/SaxoCom/saxo-currencies.git@0.0.1-beta


* **Through** _requirements.txt_. Add one of these lines to the `requirements.txt` file:

       To install from the most recent code:

            git+ssh://git@github.com/SaxoCom/saxo-currencies.git

       From a specific version

            git+ssh://git@github.com/SaxoCom/saxo-currencies.git@<release-tag>

            e.g: git+ssh://git@github.com/SaxoCom/saxo-currencies.git@0.0.1-beta

    and run: `pip install -r requirements.txt`

* **Downloading the code**

       Download the lib code. Then run this command from the command prompt:

            python setup.py install


# Run the tests suite

    pip install nose

    nosetests tests
