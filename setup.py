# -*- coding: utf-8 -*-

from setuptools import setup, find_packages


with open('README.md') as f:
    readme = f.read()

with open('LICENSE.txt') as f:
    license_content = f.read()

excluded = []


def exclude_package(pkg):
    for exclude in excluded:
        if pkg.startswith(exclude):
            return True
    return False


def create_package_list(base_package):
    return ([base_package] +
            [base_package + '.' + pkg
             for pkg
             in find_packages(base_package)
             if not exclude_package(pkg)])

setup(
    name='saxo-currencies',
    version='0.0.3',
    description='A Python utility for currency handling and conversions, '
                'using the Saxo Currency Service.',
    long_description=readme,
    author='Saxo Publish',
    author_email='publish@saxo.com',
    url='https://saxo.githost.io/publish/saxo-currencies',
    license=license_content,
    packages=create_package_list('saxocurrencies'),
    install_requires=[
        "mock>=2.0.0",
        "py-moneyed>=0.7.0",
        "requests>=2.18.4",
    ],
    keywords=["currencies", ],
    classifiers=[
        "Development Status :: 4 - Beta",
        "Intended Audience :: Developers",
        "Natural Language :: English",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
        "Programming Language :: Python",
        "Programming Language :: Python :: 2",
        "Programming Language :: Python :: 2.6",
        "Programming Language :: Python :: 2.7",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3.3",
        "Programming Language :: Python :: 3.4",
        "Programming Language :: Python :: 3.5",
        "Programming Language :: Python :: Implementation :: CPython",
        "Programming Language :: Python :: Implementation :: PyPy",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ]
)
