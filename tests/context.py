# -*- coding: utf-8 -*-

import sys
import os
sys.path.insert(0, os.path.abspath(os.path.join(os.path.dirname(__file__), '..')))

from saxocurrencies import (
    Currencies,
    CurrencyDoesNotExist,
)

currencies = Currencies(
    'http://api.saxoudv.com/v1/',
    'testkey'
)
