# -*- coding: utf-8 -*-
import unittest
from mock import mock
from moneyed import (
    Money,
    USD,
    DKK,
    FJD,
)
from decimal import Decimal

from .context import (
    currencies,
    CurrencyDoesNotExist,
)
from .utils import (
    mock_request_get,
    mock_currency_id2,
    mock_requests_get_currency_data_all_dkk,
    mock_requests_get_currency_data_all_usd,
    CURRENCIES_LIST,
)


class ConverterTestSuite(unittest.TestCase):
    USD_SAXO_CURRENCY_ID = '5'
    DKK_SAXO_CURRENCY_ID = '1'

    @mock.patch('requests.get', mock_request_get)
    def test_get_exchange_rate_from_saxo(self):
        self.assertRaises(
            CurrencyDoesNotExist,
            currencies.get_currency_by_code,
            FJD,
        )
        usd_data = currencies.get_currency_by_code(USD)
        self.assertEqual(usd_data['exchangerate'], '652.17')
        self.assertEqual(usd_data['id'], '5')

        dkk_data = currencies.get_currency_by_code(DKK)
        self.assertEqual(dkk_data['exchangerate'], '100.00')
        self.assertEqual(dkk_data['id'], '1')

    @mock.patch(
        'requests.get',
        mock_currency_id2
    )
    def test_get_shop_currency_by_id(self):
        currency_data = currencies.get_currency_by_id(
            self.DKK_SAXO_CURRENCY_ID
        )

        self.assertEqual(
            currency_data['alias'],
            'DKK'
        )
        self.assertEqual(
            str(currency_data['id']),
            str(self.DKK_SAXO_CURRENCY_ID)
        )

    @mock.patch(
        'requests.get',
        mock_requests_get_currency_data_all_dkk
    )
    def test_get_currency_code_by_id_dkk(self):
        currency_data = currencies.get_currency_code_by_id(
            self.DKK_SAXO_CURRENCY_ID
        )

        self.assertEqual(
            currency_data,
            'DKK'
        )

    @mock.patch(
        'requests.get',
        mock_requests_get_currency_data_all_usd
    )
    def test_get_currency_code_by_id_usd(self):
        currency_data = currencies.get_currency_code_by_id(
            self.USD_SAXO_CURRENCY_ID
        )

        self.assertEqual(
            currency_data,
            'USD'
        )

    @mock.patch('requests.get', mock_requests_get_currency_data_all_dkk)
    def test_convert_dkk_to_dkk(self):
        original_amount = Money(100, DKK)

        # With the same currency as the active publish front the value should
        # not change
        self.assertEqual(
            currencies.convert(
                original_amount,
                DKK
            ),
            original_amount
        )

    @mock.patch('requests.get', mock_requests_get_currency_data_all_usd)
    def test_convert_dkk_to_usd(self):
        original_amount = Money(100, DKK)
        # No let's change the publish front
        converted_amount = currencies.convert(
            original_amount,
            USD,
        )
        self.assertEqual(
            str(converted_amount.currency),
            str(USD)
        )
        self.assertEqual(
            round(float(converted_amount.amount), 2),
            15.33
        )

    @mock.patch('requests.get', mock_requests_get_currency_data_all_dkk)
    def test_convert_usd_to_dkk(self):
        original_amount = Money(100, USD)

        converted_money = currencies.convert(
            original_amount,
            DKK,
        )
        self.assertEqual(
            str(converted_money.currency),
            'DKK'
        )
        self.assertEqual(
            round(float(converted_money.amount), 2),
           652.17
        )

    @mock.patch('requests.get', mock_requests_get_currency_data_all_usd)
    def test_convert_usd_to_usd(self):
        original_amount = Money(100, USD)

        # With the same currency as the active publish front the value should
        # not change
        self.assertEqual(
            currencies.convert(
                original_amount,
                USD,
            ),
            original_amount
        )

    @mock.patch('requests.get', mock_currency_id2)
    def test_convert_sek_to_usd(self):
        original_amount = Money(100, 'SEK')

        converted_money = currencies.convert(
            original_amount,
            USD,
        )
        self.assertEqual(
            str(converted_money.currency),
            'USD'
        )
        # SEK exchange rate = 80.53
        # USD exchange rate = 652.17
        # Calculation: 100 * 80.53 / 652.17 = 12.348007421377863
        self.assertEqual(
            round(float(converted_money.amount), 2),
            12.35
        )

    @mock.patch('requests.get', mock_currency_id2)
    def test_convert_round(self):
        # Integer value
        original_amount = Money(100, 'SEK')

        converted_money = currencies.convert(
            original_amount,
            USD,
        )
        self.assertEqual(
            str(converted_money.currency),
            'USD'
        )
        # SEK exchange rate = 80.53
        # USD exchange rate = 652.17
        # Calculation: 100 * 80.53 / 652.17 = 12.348007421377863
        # Rounding both values to two floating places
        self.assertEqual(
            converted_money.amount.quantize(Decimal('.01')),
            Decimal(12.35).quantize(Decimal('.01'))
        )

        # Float value
        original_amount = Money(100.0, 'SEK')

        converted_money = currencies.convert(
            original_amount,
            USD,
        )
        self.assertEqual(
            str(converted_money.currency),
            'USD'
        )
        # SEK exchange rate = 80.53
        # USD exchange rate = 652.17
        # Calculation: 100 * 80.53 / 652.17 = 12.348007421377863
        # Rounding both values to two floating places
        self.assertEqual(
            converted_money.amount.quantize(Decimal('.01')),
            Decimal(12.35).quantize(Decimal('.01'))
        )

    def test_convert_from_currency_data_dkk_to_dkk(self):
        """Test we get correct amount for DKK -> DKK."""
        amount_money = Money(100, 'DKK')
        convert_to_currency = DKK
        # Grab the currency data for DKK
        convert_to_currency_data = [
            c for c in CURRENCIES_LIST
            if c["alias"] == str(convert_to_currency)
        ][0]
        amount_money_currency_data = convert_to_currency_data

        converted_money = currencies.convert_from_currency_data(
            amount_money,
            convert_to_currency_data,
            amount_money_currency_data
        )
        # The amount to be converted and converted amount should be equal.
        self.assertEqual(converted_money, amount_money)

    def test_convert_from_currency_data_dkk_to_usd(self):
        """Test we get correct amount for DKK -> USD."""
        amount_money = Money(100, 'DKK')
        convert_to_currency = USD
        # Grab the currency data for DKK and USD.
        convert_to_currency_data = [
            c for c in CURRENCIES_LIST
            if c["alias"] == str(convert_to_currency)
        ][0]
        amount_money_currency_data = [
            c for c in CURRENCIES_LIST
            if c["alias"] == str(amount_money.currency)
        ][0]

        converted_money = currencies.convert_from_currency_data(
            amount_money,
            convert_to_currency_data,
            amount_money_currency_data
        )
        # DKK exchange rate = 100.0
        # USD exchange rate = 652.17
        # Calculation: 100 * 100 / 652.17 = 15.333425333885337
        # Rounding both values to two floating places
        self.assertEqual(
            converted_money.amount.quantize(Decimal("0.01")),
            Decimal(15.33).quantize(Decimal("0.01"))
        )


if __name__ == '__main__':
    unittest.main()
