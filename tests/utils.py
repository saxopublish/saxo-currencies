# -*- coding: utf-8 -*-
import json


CURRENCIES_LIST = [
    {
        "id": "1",
        "alias": "DKK",
        "name": "Danske kroner",
        "format": "kr. {0}",
        "exchangerate": "100.00",
        "exchangerateid": "2828"
    },
    {
        "id": "2",
        "alias": "SEK",
        "name": "Svenske kroner",
        "format": "kr. {0}",
        "exchangerate": "80.53",
        "exchangerateid": "2828"
    },
    {
        "id": "3",
        "alias": "EUR",
        "name": "Euro",
        "format": "€{0}",
        "exchangerate": "743.53",
        "exchangerateid": "2828"
    },
    {
        "id": "4",
        "alias": "GBP",
        "name": "Engelske pund",
        "format": "£{0}",
        "exchangerate": "950.48",
        "exchangerateid": "2828"
    },
    {
        "id": "5",
        "alias": "USD",
        "name": "Amerikanske dollar",
        "format": "${0}",
        "exchangerate": "652.17",
        "exchangerateid": "2828"
    },
    {
        "id": "276",
        "alias": "NOK",
        "name": "Norske kroner",
        "format": "Kr. {0}",
        "exchangerate": "80.76",
        "exchangerateid": "2828"
    },
    {
        "id": "277",
        "alias": "ARS",
        "name": "Argentinske peso",
        "format": "$a{0}",
        "exchangerate": "46.97",
        "exchangerateid": "2828"
    },
    {
        "id": "278",
        "alias": "BOB",
        "name": "Boliviano",
        "format": "Bs{0}",
        "exchangerate": "94.72",
        "exchangerateid": "2828"
    },
    {
        "id": "279",
        "alias": "CLP",
        "name": "Chilenske peso",
        "format": "${0}",
        "exchangerate": "0.96",
        "exchangerateid": "2828"
    },
    {
        "id": "280",
        "alias": "COP",
        "name": "Colombianske peso",
        "format": "COL${0}",
        "exchangerate": "0.23",
        "exchangerateid": "2828"
    },
    {
        "id": "281",
        "alias": "CRC",
        "name": "Costaricanske colón",
        "format": "CRC{0}",
        "exchangerate": "1.21",
        "exchangerateid": "2828"
    },
    {
        "id": "282",
        "alias": "DOP",
        "name": "Dominikanske peso",
        "format": "RD${0}",
        "exchangerate": "14.20",
        "exchangerateid": "2828"
    },
    {
        "id": "283",
        "alias": "HNL",
        "name": "Lempira",
        "format": "L{0}",
        "exchangerate": "28.71",
        "exchangerateid": "2828"
    },
    {
        "id": "284",
        "alias": "MXN",
        "name": "Mexicanske peso",
        "format": "MEX${0}",
        "exchangerate": "35.96",
        "exchangerateid": "2828"
    },
    {
        "id": "285",
        "alias": "NIO",
        "name": "Cordoba Oro",
        "format": "C${0}",
        "exchangerate": "22.86",
        "exchangerateid": "2828"
    },
    {
        "id": "286",
        "alias": "PAB",
        "name": "Balboa",
        "format": "B/.{0}",
        "exchangerate": "652.45",
        "exchangerateid": "2828"
    },
    {
        "id": "287",
        "alias": "PEN",
        "name": "Ny sol",
        "format": "S/.{0}",
        "exchangerate": "197.66",
        "exchangerateid": "2828"
    },
    {
        "id": "288",
        "alias": "PYG",
        "name": "Guarani",
        "format": "G{0}",
        "exchangerate": "0.12",
        "exchangerateid": "2828"
    },
    {
        "id": "289",
        "alias": "VEF",
        "name": " Bolivar",
        "format": "Bs{0}",
        "exchangerate": "65.54",
        "exchangerateid": "2828"
    }
]


def mock_request_get(uri):
    class Content(object):
        pass
    c = Content()
    c.content = json.dumps({
        "currencies": CURRENCIES_LIST
    })
    c.json = lambda: json.loads(c.content)

    return c


def mock_currency_id2(uri):
    class Response(object):
        def __init__(self, status_code, content=''):
            self.status_code = status_code
            self.content = content

    response = Response(200)
    response.content = json.dumps({
        "currencies": CURRENCIES_LIST
    })
    response.json = lambda: json.loads(response.content)

    return response


def mock_requests_get_currency_data_all_dkk(uri):
    class Content(object):
        pass
    c = Content()
    c.content = \
        '{"currencies":[{"id":"1","alias":"DKK","name":"Danske kroner",' \
        '"format":"kr. {0}","exchangerate":"100.00","exchangerateid":"282' \
        '8"}, {"id":"5","alias":"USD","name":"Amerikanske dollar' \
        '","format":"${0}","exchangerate":"652.17","exchangerateid":"2828' \
        '"}], "paymentoptions":[{"id":"1-1","name":"VISA-Dankort",' \
        '"currencyid":"1","vatpercent":"0.00","vatamount":"0.00",' \
        '"pricetotalexvat":"0.00","pricetotalinclvat":"0.00",' \
        '"paymentmethodid":1,"paymenttype":"VisaDankort"},{"id":"8-3",' \
        '"name":"VISA, MasterCard, Eurocard, Diners","currencyid":"1",' \
        '"vatpercent":"0.00","vatamount":"0.00","pricetotalexvat":"23.85",' \
        '"pricetotalinclvat":"23.85","paymentmethodid":8,' \
        '"paymenttype":"Creditcard"},{"id":"11-7","name":"MobilePay",' \
        '"currencyid":"1","vatpercent":"0.00","vatamount":"0.00",' \
        '"pricetotalexvat":"0.00","pricetotalinclvat":"0.00",' \
        '"paymentmethodid":11,"paymenttype":"MobilePay"},{"id":"12-8",' \
        '"name":"ViaBill","currencyid":"1","vatpercent":"0.00",' \
        '"vatamount":"0.00","pricetotalexvat":"0.00",' \
        '"pricetotalinclvat":"0.00","paymentmethodid":12,' \
        '"paymenttype":"ViaBill"}]}'
    c.status_code = 200
    c.json = lambda: json.loads(c.content)

    return c


def mock_requests_get_currency_data_all_usd(uri):
    class Content(object):
        pass
    c = Content()
    c.content = \
        '{"currencies":[{"id":"5","alias":"USD","name":"Amerikanske dollar' \
        '","format":"${0}","exchangerate":"652.17","exchangerateid":"2828' \
        '"}, {"id":"1","alias":"DKK","name":"Danske kroner",' \
        '"format":"kr. {0}","exchangerate":"100.00","exchangerateid":"282' \
        '8"}], "paymentoptions":[{"id":"1-1","name":"VISA-Dankort",' \
        '"currencyid":"5","vatpercent":"0.00","vatamount":"0.00",' \
        '"pricetotalexvat":"0.00","pricetotalinclvat":"0.00",' \
        '"paymentmethodid":4,"paymenttype":"VisaDankort"},{"id":"8-3",' \
        '"name":"VISA, MasterCard, Eurocard, Diners","currencyid":"5",' \
        '"vatpercent":"0.00","vatamount":"0.00","pricetotalexvat":"23.85",' \
        '"pricetotalinclvat":"23.85","paymentmethodid":8,' \
        '"paymenttype":"Creditcard"},{"id":"11-7","name":"MobilePay",' \
        '"currencyid":"5","vatpercent":"0.00","vatamount":"0.00",' \
        '"pricetotalexvat":"0.00","pricetotalinclvat":"0.00",' \
        '"paymentmethodid":11,"paymenttype":"MobilePay"},{"id":"12-8",' \
        '"name":"ViaBill","currencyid":"5","vatpercent":"0.00",' \
        '"vatamount":"0.00","pricetotalexvat":"0.00",' \
        '"pricetotalinclvat":"0.00","paymentmethodid":12,' \
        '"paymenttype":"ViaBill"}]}'
    c.status_code = 200
    c.json = lambda: json.loads(c.content)

    return c
